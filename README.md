# DoDeDoDo Web Interface

Locally hosted web version of the reference stats collector 'DoDeDoDo'.

Please refere to this post for pre-setup instructions https://shaddowland.net/2018/07/24/dodedodo-a-location-based-enquiry-tracking-system/ (follow steps 1-4 for web only)

Save the html and css files into a shared directory. If you can use any other browswer than IE use the indicated html file as this uses a better method of GET request. If you can only use IE see below.

Replace <your devID> with your Pushingbox.com device ID and <Library> with your library branch name, and replace the logo file with your own logo.

<b>Can only use IE</b>

If you can only use IE you can disable same origin policy https://stackoverflow.com/questions/20947359/disable-same-origin-policy-internet-explorer to bypass CORS.
Make sure you use the IE only version of the code as the fetch() requests will not work in IE.

Run and enjoy.